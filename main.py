import os
import sys
import logging
from os.path import join

from src.ArgParser import ArgParser 
from src.ActionLoader import ActionLoader
from src.AbstractAction import AbstractAction
from src.ConfigurationHandler import ConfigurationHandler


if __name__ == '__main__':
  ActionsLoader( sys.argv[1:] )